﻿using UnityEngine;

public class UI: MonoBehaviour
{
   public virtual void  Show()
    {
        gameObject.SetActive(true);
    }
    
    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}