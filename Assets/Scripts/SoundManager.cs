using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] private AudioClip scoreNormal;
    [SerializeField] private AudioClip scoreCombo;
    [SerializeField] private AudioClip wrongMove;
    [SerializeField] private AudioClip tap;

    private AudioSource m_player;

    private void Start()
    {
        m_player = GetComponent<AudioSource>();
    }

    public void PlayScore(bool isCombo)
    {
        m_player.PlayOneShot(isCombo ? scoreCombo : scoreNormal);
    }

    public void PlayWrong()
    {
        m_player.PlayOneShot(wrongMove);
    }

    public void PlayTap()
    {
        m_player.PlayOneShot(tap);
    }
}
