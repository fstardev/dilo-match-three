using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : Singleton<TimeManager>
{
    [SerializeField] private int duration;
    
    private float m_time;

    private void Start()
    {
        m_time = 0;
    }

    private void Update()
    {
        if(GameFlowManager.Instance.IsGameOver) return;

        if (m_time > duration)
        {
            GameFlowManager.Instance.GameOver();
            return;
        }

        m_time += Time.deltaTime;
    }

    public float GetRemainingTime()
    {
        return duration - m_time;
    }
}
