using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class BoardManager: Singleton<BoardManager>
{
   [Header("Board")]
   [SerializeField] private Vector2Int size;
   [SerializeField] private Vector2 offsetTile;
   [SerializeField] private Vector2 offsetBoard;

   [Header("Tile")]
   [SerializeField]
   public List<Sprite> tileTypes = new List<Sprite>();
   [SerializeField] private GameObject tilePrefab;

   private Vector2 m_startPosition;
   private Vector2 m_endPosition;
   private TileController[,] m_tiles;
   private int m_combo;
   
   public bool IsSwapping { get; set; }
   public bool IsProcessing { get; set; }

   public bool IsAnimating => IsSwapping || IsSwapping;

   private void Start()
   {
      var tileSize = tilePrefab.GetComponent<SpriteRenderer>().size;
      CreateBoard(tileSize);

      IsSwapping = false;
      IsProcessing = false;
   }

   private void CreateBoard(Vector2 tileSize)
   {
      m_tiles = new TileController[size.x ,size.y];
      var totalSize = (tileSize + offsetTile) * (size - Vector2.one);

      m_startPosition = (Vector2) transform.position - (totalSize / 2) + offsetBoard;
      m_endPosition = m_startPosition + totalSize;

      for (var x = 0; x < size.x; x++)
      {
         for (var y = 0; y < size.y; y++)
         {
            var newTile =
               Instantiate(tilePrefab,
                     new Vector2(m_startPosition.x + ((tileSize.x + offsetTile.x) * x),
                        m_startPosition.y + ((tileSize.y + offsetTile.y) * y)), tilePrefab.transform.rotation,
                     transform)
                  .GetComponent<TileController>();
            m_tiles[x, y] = newTile;

            var possibleId = GetStartingPossibleIdList(x, y);
            var newId = possibleId[Random.Range(0, possibleId.Count)];
            newTile.ChangeId(newId, x, y);
         }
      }
   }

   private List<int> GetStartingPossibleIdList(int x, int y)
   {
      var possibleId = new List<int>();
      for (var i = 0; i < tileTypes.Count; i++)
      { 
         possibleId.Add(i);
      }

      if (x > 1 && m_tiles[x - 1, y].ID == m_tiles[x - 2, y].ID)
      {
         possibleId.Remove(m_tiles[x - 1, y].ID);
      }
      
      if (y > 1 && m_tiles[x, y- 1].ID == m_tiles[x, y - 2].ID)
      {
         possibleId.Remove(m_tiles[x, y - 1].ID);
      }

      return possibleId;
   }

   public void Process()
   {
      m_combo = 0;
      IsProcessing = true;
      ProcessMatch();
   }

   #region Swapping

   public IEnumerator SwapTilePosition(TileController a, TileController b, Action onCompleted)
   {
      IsSwapping = true;

      var indexA = GetTileIndex(a);
      var indexB = GetTileIndex(b);

      m_tiles[indexA.x, indexA.y] = b;
      m_tiles[indexB.x, indexB.y] = a;
      
      a.ChangeId(a.ID, indexB.x, indexB.y);
      b.ChangeId(b.ID, indexA.x, indexA.y);

      var isRoutineACompleted = false;
      var isRoutineBCompleted = false;

      StartCoroutine(a.MoveTilePosition(GetIndexPosition(indexB), () => isRoutineACompleted = true));
      StartCoroutine(b.MoveTilePosition(GetIndexPosition(indexA), () => isRoutineBCompleted = true));

      yield return new WaitUntil(() => isRoutineACompleted && isRoutineBCompleted);
      onCompleted?.Invoke();
      IsSwapping = false;
   }

   private Vector2 GetIndexPosition(Vector2Int index)
   {
      var tileSize = tilePrefab.GetComponent<SpriteRenderer>().size;
      return new Vector2(m_startPosition.x + ((tileSize.x + offsetTile.x) * index.x),
         m_startPosition.y + ((tileSize.y + offsetTile.y) * index.y));
   }

   private Vector2Int GetTileIndex(Object tile)
   {
      for (var x = 0; x < size.x; x++)
      {
         for (var y = 0; y < size.y; y++)
         {
            if (tile == m_tiles[x, y]) return new Vector2Int(x, y);
         }
      }

      return new Vector2Int(-1, -1);
   }

   #endregion

   #region Matches

   private void ProcessMatch()
   {
     
      var matchingTiles = GetAllMatches();
      if (matchingTiles == null || matchingTiles.Count == 0)
      {
         IsProcessing = false;
         return;
      }

      m_combo++;
      ScoreManager.Instance.IncrementCurrentScore(matchingTiles.Count, m_combo);
      StartCoroutine(ClearMatches(matchingTiles, ProcessDrop));
      
   }

   private void ProcessDrop()
   {
      var droppingTiles = GetAllDrop();
      StartCoroutine(DropTiles(droppingTiles, ProcessDestroyAndFill));
   }
   
   private IEnumerator ClearMatches(IReadOnlyList<TileController> matchingTiles, Action onCompleted)
   {
      var isCompleted = new List<bool>();
      for (var i = 0; i < matchingTiles.Count; i++)
      {
         isCompleted.Add(false);
      }

      for (var i = 0; i < matchingTiles.Count; i++)
      {
         var index = i;
         StartCoroutine(matchingTiles[i].SetDestroyed(() => isCompleted[index] = true));
      }

      yield return new WaitUntil(() => IsAllTrue(isCompleted));
      onCompleted?.Invoke();
   }
   
   private Dictionary<TileController, int> GetAllDrop()
   {
      var droppingTiles = new Dictionary<TileController, int>();
      for (var x = 0; x < size.x; x++)
      {
         for (var y = 0; y < size.y; y++)
         {
            if (!m_tiles[x, y].IsDestroyed) continue;
            for (var i = y+1; i < size.y; i++)
            {
               if(m_tiles[x, i].IsDestroyed) continue;
                  
               if (droppingTiles.ContainsKey(m_tiles[x, i]))
               {
                  droppingTiles[m_tiles[x, i]]++;
               }
               else
               {
                  droppingTiles.Add(m_tiles[x, i], 1);
               }
            }
         }
      }

      return droppingTiles;
   }

   private IEnumerator DropTiles(Dictionary<TileController, int> droppingTiles, Action onCompleted)
   {
      foreach (var pair in droppingTiles)
      {
         var tileIndex = GetTileIndex(pair.Key);

         var temp = pair.Key;
         m_tiles[tileIndex.x, tileIndex.y] = m_tiles[tileIndex.x, tileIndex.y - pair.Value];
         m_tiles[tileIndex.x, tileIndex.y - pair.Value] = temp;
         
         temp.ChangeId(temp.ID, tileIndex.x, tileIndex.y - pair.Value);
      }

      yield return null;
      onCompleted?.Invoke();
   }

   #endregion

   #region Destroy & Fill

   private void ProcessDestroyAndFill()
   {
      var destroyedTiles = GetAllDestroyed();
      StartCoroutine(DestroyAndFillTiles(destroyedTiles, ProcessReposition));
   }

   private IEnumerable<TileController> GetAllDestroyed()
   {
      var destroyedTiles = new List<TileController>();
      for (var x = 0; x < size.x; x++)
      {
         for (var y = 0; y < size.y; y++)
         {
            if (m_tiles[x,y].IsDestroyed)
            {
               destroyedTiles.Add(m_tiles[x,y]);
            }
         }
      }

      return destroyedTiles;
   }

   private IEnumerator DestroyAndFillTiles(IEnumerable<TileController> destroyedTiles, Action onCompleted)
   {
      var highestIndex = new List<int>();
      for (var i = 0; i < size.x; i++)
      {
         highestIndex.Add(size.y-1);
      }

      var spawnHeight = m_endPosition.y + tilePrefab.GetComponent<SpriteRenderer>().size.y + offsetTile.y;
      foreach (var tile in destroyedTiles)
      {
         var tileIndex = GetTileIndex(tile);
         var targetIndex = new Vector2Int(tileIndex.x, highestIndex[tileIndex.x]);
         highestIndex[tileIndex.x]--;

         var tileTransform = tile.transform;
         tileTransform.position = new Vector2(tileTransform.position.x, spawnHeight);
         tile.GenerateRandomTile(targetIndex.x, targetIndex.y);
      }

      yield return null;
      onCompleted?.Invoke();
   }

   #endregion

   #region Reposition

   private void ProcessReposition()
   {
      StartCoroutine(RepositionTiles(ProcessMatch));
   }

   private IEnumerator RepositionTiles(Action onCompleted)
   {
      var isCompleted = new List<bool>();

      var i = 0;
      for (var x = 0; x < size.x; x++)
      {
         for (var y = 0; y < size.y; y++)
         {
            var targetPosition = GetIndexPosition(new Vector2Int(x, y));
            if ((Vector2)m_tiles[x,y].transform.position == targetPosition) continue; 
            isCompleted.Add(false);
            var index = i;
            StartCoroutine(m_tiles[x, y].MoveTilePosition(targetPosition, () => isCompleted[index] = true));
            i++;
         }
      }

      yield return new WaitUntil(() => IsAllTrue(isCompleted));
      onCompleted?.Invoke();
   }

   #endregion
   private static bool IsAllTrue(IEnumerable<bool> list)
   {
      return list.All(status => status);
   }
   public List<TileController> GetAllMatches()
   {
      var matchingTiles = new List<TileController>();
      for (var x = 0; x < size.x; x++)
      {
         for (var y = 0; y < size.y; y++)
         {
            var tileMatched = m_tiles[x, y].GetAllMatches();
            if (tileMatched == null || tileMatched.Count == 0)
            {
               continue;
            }

            foreach (var tile in tileMatched.Where(tile => !matchingTiles.Contains(tile)))
            {
               matchingTiles.Add(tile);
            }
         }
      }

      return matchingTiles;
   }
}
