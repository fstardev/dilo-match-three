using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScore : UI
{
    [SerializeField] private Text highScore;
    [SerializeField] private Text currentScore;

    private void Update()
    {
        highScore.text = $"HI Score: {ScoreManager.Instance.HighScore}";
        currentScore.text = $"Score: {ScoreManager.Instance.CurrentScore}";
    }
}
