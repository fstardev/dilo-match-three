using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileController : MonoBehaviour
{
    private static readonly Vector2[] AdjacentDirection = new Vector2[]
        {Vector2.up, Vector2.down, Vector2.left, Vector2.right,};
    private static readonly Color SelectedColor = new Color(.5f, .5f, .5f);
    private static readonly Color NormalColor = Color.white;
    
    private static readonly float MoveDuration = .5f;
    private static readonly float DestroyBigDuration = .1f;
    private static readonly float DestorySmallDuration = .4f;

    private static readonly Vector2 SizeBig = Vector2.one * 1.2f;
    private static readonly Vector2 SizeSmall = Vector2.zero;
    private static readonly Vector2 SizeNormal = Vector2.one;

  
    private static TileController _previousSelected = null;
    
    private BoardManager m_board;
    private SpriteRenderer m_render;
    private bool m_isSelected;
    private GameFlowManager m_game;
    public int ID { get; private set; }
    public bool IsDestroyed { get; private set; }
    
    private void Awake()
    {
        m_board = BoardManager.Instance;
        m_render = GetComponent<SpriteRenderer>();
        m_game = GameFlowManager.Instance;
    }

    private void Start()
    {
        IsDestroyed = false;
    }

    private void OnMouseDown()
    {
        if(m_render.sprite == null || m_board.IsAnimating || m_game.IsGameOver) return;

        SoundManager.Instance.PlayTap();
        
        if (m_isSelected)
        {
            Deselect();
        }
        else
        {
            if (_previousSelected == null)
            {
                Select();
            }
            else
            {
                if (GetAllAdjacentTiles().Contains(_previousSelected))
                {
                    var otherTile = _previousSelected;
                    _previousSelected.Deselect();
                    
                    SwapTile(otherTile, () =>
                    {
                        if (m_board.GetAllMatches().Count > 0)
                        {
                            print("MATCH FOUND");
                            m_board.Process();
                        }
                        else
                        {
                            SoundManager.Instance.PlayWrong();
                            SwapTile(otherTile);
                        }
                    });
                }
                else
                {
                    _previousSelected.Deselect();
                    Select();
                }
                
            }
        }
    }

    public IEnumerator SetDestroyed(Action onCompleted)
    {
        IsDestroyed = true;
        ID = -1;
        name = "TILE_NULL";

        var startSize = transform.localScale;
        var time = 0f;

        while (time < DestroyBigDuration)
        {
            transform.localScale = Vector2.Lerp(startSize, SizeBig, time / DestroyBigDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = SizeSmall;
        m_render.sprite = null;
        onCompleted?.Invoke();
    }

    private void SwapTile(TileController otherTile, Action onCompleted = null)
    {
        StartCoroutine(m_board.SwapTilePosition(this, otherTile, onCompleted));
    }

    public IEnumerator MoveTilePosition(Vector2 targetPosition, Action onCompleted)
    {
        var startPosition = transform.position;
        var time = 0.0f;

        yield return new WaitForEndOfFrame();

        while (time < MoveDuration)
        {
            transform.position = Vector2.Lerp(startPosition, targetPosition, time / MoveDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.position = targetPosition;
        onCompleted?.Invoke();
    }

    public void GenerateRandomTile(int x, int y)
    {
        transform.localScale = SizeNormal;
        IsDestroyed = false;
        
        ChangeId(Random.Range(0, m_board.tileTypes.Count), x, y);
    }

    public void ChangeId(int id, int x, int y)
    {
        m_render.sprite = m_board.tileTypes[id];
        ID = id;
        name = $"TILE_{ID} ({x},{y})";
    }

    #region Select & Deselect

    private void Select()
    {
        m_isSelected = true;
        m_render.color = SelectedColor;
        _previousSelected = this;
    }

    private void Deselect()
    {
        m_isSelected = false;
        m_render.color = NormalColor;
        _previousSelected = null;    
    }

    #endregion

    #region Adjacent

    private TileController GetAdjacent(Vector2 castDir)
    {
        var hit = Physics2D.Raycast(transform.position, castDir, m_render.size.x);
        return hit ? hit.collider.GetComponent<TileController>() : null;
    }

    private List<TileController> GetAllAdjacentTiles()
    {
        var adjacentTiles = AdjacentDirection.Select(GetAdjacent).ToList();
        return adjacentTiles;
    }

    #endregion

    #region Check Match
    private IEnumerable<TileController> GetMatch(Vector2 castDir)
    {
        var matchingTiles = new List<TileController>();
        var hit = Physics2D.Raycast(transform.position, castDir, m_render.size.x);

        while (hit)
        {
            var otherTile = hit.collider.GetComponent<TileController>();
            if (otherTile.ID != ID || otherTile.IsDestroyed)
            {
                break;
            }
            
            matchingTiles.Add(otherTile);
            hit = Physics2D.Raycast(otherTile.transform.position, castDir, m_render.size.x);
        }

        return matchingTiles;
    }

    private List<TileController> GetOneLineMatch(IEnumerable<Vector2> paths)
    {
        var matchingTiles = new List<TileController>();
        foreach (var tile in paths)
        {
            matchingTiles.AddRange(GetMatch(tile));
        }

        return matchingTiles.Count >= 2 ? matchingTiles : null;
    }

    public List<TileController> GetAllMatches()
    {
        if (IsDestroyed) return null;
        var matchingTiles = new List<TileController>();
        var horizontalMatchingTiles = GetOneLineMatch(new Vector2[2] {Vector2.left, Vector2.right});
        var verticalMatchingTiles = GetOneLineMatch(new Vector2[2] {Vector2.up, Vector2.down});

        if (horizontalMatchingTiles != null)
        {
            matchingTiles.AddRange(horizontalMatchingTiles);
        }

        if (verticalMatchingTiles != null)
        {
            matchingTiles.AddRange(verticalMatchingTiles);
        }

        if (matchingTiles.Count >= 2)
        {
            matchingTiles.Add(this);
        }

        return matchingTiles;
    }

    #endregion
    
}
