using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : Singleton<ScoreManager>
{
    private static int _highScore;
    
    [SerializeField] private int tileRatio;
    [SerializeField] private int comboRatio;

    public int CurrentScore { get; private set; }

    public int HighScore => _highScore;

    private void Start()
    {
        ResetCurrentScore();
    }

    private void ResetCurrentScore()
    {
        CurrentScore = 0;
    }

    public void IncrementCurrentScore(int tileCount, int comboCount)
    {
        CurrentScore += (tileCount * tileRatio) * (comboCount * comboRatio);
        SoundManager.Instance.PlayScore(comboCount > 1);
    }

    public void SetHighScore()
    {
        _highScore = Mathf.Max(CurrentScore, _highScore);
    }
}
