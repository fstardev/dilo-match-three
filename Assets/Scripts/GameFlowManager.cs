using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFlowManager : Singleton<GameFlowManager>
{
    [Header("UI")] [SerializeField] private UIGameOver gameOverUI;
    
    public bool IsGameOver { get; private set; }

    private void Start()
    {
        IsGameOver = false;
    }

    public void GameOver()
    {
        IsGameOver = true;
        ScoreManager.Instance.SetHighScore();
        gameOverUI.Show();
    }
}
