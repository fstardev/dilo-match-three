using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITime : UI
{
    [SerializeField] private Text time;
    
    private void Update()
    {
        time.text = $"{GetTimeString(TimeManager.Instance.GetRemainingTime() + 1)}";
    }

    private static string GetTimeString(float timeRemaining)
    {
        var minute = Mathf.FloorToInt(timeRemaining / 60);
        var second = Mathf.FloorToInt(timeRemaining % 60);

        return $"{minute:d2}:{second:d2}";
    }
}
